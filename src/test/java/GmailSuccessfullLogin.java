import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

/**
 * Created by P7110262 on 9/15/2015.
 */
public class GmailSuccessfullLogin {

    @Test
    public void loginSuccess() {
        // 1. Open gmail login page
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://accounts.google.com/");

        // 2.Enter username
        WebElement usernameTextBox = driver.findElement(By.id("Email"));
        usernameTextBox.clear();
        usernameTextBox.sendKeys("roshantest79@gmail.com");

        //3. Click on 'Next' Button
        WebElement nextButton = driver.findElement(By.id("next"));
        nextButton.click();

        //4. Enter Password
        WebElement passwordTextBox = driver.findElement(By.id("Passwd"));
        passwordTextBox.sendKeys("Roshan@123");

        //5. Click on 'Sign in' button
        WebElement signInButton = driver.findElement(By.id("signIn"));
        signInButton.click();

        //6. Create elements for 'Apps Button' and 'Gmail Inbox Button'
        WebElement appsButton = driver.findElement(By.xpath("//a[@title='Google Apps']"));
        WebElement gmailInboxButton = driver.findElement(By.xpath("//*[@id='gb23']/span[1]"));

        // 7. Click on 'Apps Button' and then on 'Gmail Inbox button'
        Actions action = new Actions(driver);
        action.moveToElement(appsButton).click().build().perform();
        action.moveToElement(gmailInboxButton).click().build().perform();

        // 8. Validate Inbox
        Assert.assertTrue("Inbox not found", driver.findElements(By.partialLinkText("Inbox")).size()>0);

        // 9. Sign out
        WebElement userDropdown = driver.findElement(By.cssSelector("a[class='gb_b gb_Ra gb_R gb_Na']"));
        userDropdown.click();

        WebElement signOutButton = driver.findElement(By.id("gb_71"));
        signOutButton.click();

        //10. Validate Sign out
        Assert.assertTrue("Logout unsuccessfull" , driver.findElement(By.id("profile-img")).isDisplayed());




    }


}
